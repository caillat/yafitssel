var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const config = require('config');

const OLQV = config.OLQV;

function connectDBPromise(dbFilePath) {
  console.group('connectDBPromise');
  console.groupEnd();
  return new Promise(function (resolve, reject) {
    let db = new sqlite3.Database(dbFilePath, sqlite3.OPEN_READWRITE, (err) => {
      if (err) {
        console.error(err.message);
        reject(err.message);
      }
      else {
        console.log('Connected to the database.');
        resolve(db);
      }
    });
  })
}

function queryPromise(db, query) {
  console.group('queryPromise');
  console.groupEnd();
  return new Promise(function (resolve, reject) {
    console.log(`Sending query ${query}`);
    db.all(query, [], (err, rows) => {
      if (err) {
        console.log(err.message);
        reject(err.message);
      }
      else {
        resolve(rows);
      }
    });
  })
}

var content = "";

/* GET home page. */
router.get('/', function (req, res, next) {
  console.group("get_/")
  //res.render('index', { title: 'Express' });
  res.render('yafitssel', { queries: content });
  console.groupEnd();
});

/* POST query page */
router.post('/q', function (req, res, next) {
  console.group("post_/q");
  var history = req.body.history;
  var query = req.body.query;
  console.log(`query : ${query}`);


  // Reformat the query in a way it can be processed by sqlLite3 with the json extension.
  // Replace any pattern 'fitsinfos.Header.<id>' with json_extract(fitsinfos.Header, "$.<id>")
  //
  var pattern = /fitsinfos\.Header\.([\w-]+)/g;
  var rewritten = query.replace(pattern, "json_extract(fitsinfos.Header, '\$.$1')");

  // Combine two promises to connect and query
  //
  var result = { history: history, OLQV: OLQV };

  connectDBPromise(config.sqliteDB)
    .then((db) => queryPromise(db, rewritten), (err) => { throw (err); })
    .then((rows) => { result["OLQV"] = OLQV; result["status"] = true; result["message"] = ""; result["result"] = rows }, (err) => { throw (err); })
    .catch((err) => { result["status"] = false; result["message"] = err; })
    .then(() => {
      res.setHeader('Content-type', 'application/json');
      console.log("Sending the result");
      res.send(JSON.stringify(result));
    });

  console.groupEnd();
});

/* POST upload page */
router.post('/u', function (req, res) {
  console.group('post_/u');
  try {
    if (!req.files) {
      res.send(JSON.stringify({
        status: false,
        message: 'No file uploaded'
      }));
    }
    else {
      let file = req.files.queries;
      content = JSON.stringify(file.data.toString());
      res.redirect("/");
    }
  } catch (err) {
    res.status(500).send(err);
  }
  console.groupEnd();
});

module.exports = router;
