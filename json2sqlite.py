#!/usr/bin/env python
import getopt
import json
import sqlite3
import sysimport
import traceback
from typing import List


def main(argv: List[str]):
    try:
        print(argv[1])
    except:
        print("Bad command")
        sys.exit(2)

    try:
        conn = sqlite3.connect("fitsinfos.db")
        c = conn.cursor()
        table = "fitsinfos"
        columns_names = "Path, Size, Header"
        columns_declaration = "Path VARCHAR(200), Size BIGINT, Header JSON"
        index_declaration = "Path"
        index_columns = "Path"
        c.execute(f"DROP TABLE IF EXISTS {table}")
        c.execute(f"CREATE TABLE {table} ({columns_declaration})")
        c.execute(f"CREATE UNIQUE INDEX {index_declaration} ON {table}({index_columns})")

        with open(argv[1], "r") as json_file:
            numrows = 0
            sql = f"INSERT INTO {table}({columns_names}) VALUES (?, ?, ?)"
            print(sql)
            for json_line in json_file:
                document = json.loads(json_line)
                values = (document['Path'], document['Size'], json.dumps(document['Header']))
                try :
                    c.execute(sql, values)
                    numrows += 1
                    if numrows % 1000 == 0:
                        print(f"{numrows} created rows")
                except sqlite3.IntegrityError:
                    print(f"Doublon on {document['Path']}")

    except Exception as e:
        print(document['Path'])
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        sys.exit(2)
        
    conn.commit()
    conn.close()


if __name__ == '__main__':
    main(sys.argv)
