#!/usr/bin/env python3
#
# This script maintains an sqlite table fitsinfos updated accordingly with the collection of FITS files found under a top level directory.
#
# The sqlite table named fitsinfos has the following columns
# * the path to the fits file relative to the directory containing all the fits files (string)
# * the size as a number of bytes of the fits file ( Number )
# * a subset of the fits file header stored as an sqlite json extension
#
# The application's parameters are stored in the file constants.ini.
# Amongst those are the FITS files toplevel directory, the names of the sqlite database and table.
#
#
# Author : Michel Caillat
#
# Date : May 2020
#
import astropy.io.fits
import re
import sys
import os
import astropy
import datetime
import time
import json
from astropy.io import fits
from constants import Constants
import logging
import sqlite3
import sys
import traceback
from typing import List
from optparse import OptionParser

logging.basicConfig(format='%(asctime)s %(message)s',
                    level=logging.INFO, filename="fs2sqlite.log")

def _2uid(name):
    z = name
    if name.startswith('uid___'):
        m = re.search('^uid___A[0-9]+_X[0-9a-z]+(_X[0-9a-z]+)?', name)
        z = m.group(0).replace("___", "://").replace("_", "/")
    return z

def main():
#    logging.disable(logging.INFO)
    if not os.environ.get('__CONSTANTS__'):
        os.environ['__CONSTANTS__'] = 'DEFAULT'
    consts = Constants()

    # parser = OptionParser(
    #     usage=f"usage: %prog [options]", version="%prog 1.0")

    #options, args = parser.parse_args()

    logging.info(f"Starting to update the FITS headers table '{consts.sqlite_table}' in the sqlite data_base '{consts.sqlite_db}'")

    # Connect
    conn = sqlite3.connect(consts.sqlite_db)
    c = conn.cursor()
    table = consts.sqlite_table
    columns_names = "Path, Size, Header"
    columns_declaration = "Path VARCHAR(200), Size BIGINT, Header JSON"
    index_declaration = "Path"
    index_columns = "Path"

    # Create if needed
    c.execute(f"CREATE TABLE IF NOT EXISTS {table} ({columns_declaration})")
    c.execute(
        f"CREATE UNIQUE INDEX IF NOT EXISTS {index_declaration} ON {table}({index_columns})")
    
    ALMA_FITS_ROOT = consts.alma_fits_root

    logging.info("About to process all fits files under '%s'." % ALMA_FITS_ROOT)

    # Prepare tj
    sql = f"INSERT OR IGNORE INTO {table}({columns_names}) VALUES (?, ?, ?)"

    for root, dirs, files in os.walk(ALMA_FITS_ROOT):
        if root.find("BIG") > 1:
            continue

        for name in files:
            fullpath = ("%s/%s" % (root, name))
            #
            # Determine the path relative to ALMA_FITS_ROOT
            #
            path = fullpath[len(ALMA_FITS_ROOT)+1:]

            if name.endswith('.fits') :

                # Determine the size -> size
                #
                try :
                    size = os.path.getsize(fullpath)
                except Exception as e:
                    logging.exception(str(e))
                    logging.exception("Ignored")
                    continue
 
                #
                # Restructure the header into a dictionary
                #
                try:
                    hdulist = fits.open(fullpath)
                    header = {}
                    for k in hdulist[0].header:
                        if not k in ['HISTORY', '', 'COMMENT']:
                            try:
                                header[k] = hdulist[0].header[k]
                            except astropy.io.fits.verify.VerifyError:
                                logging.error(
                                    "Could not parse value of keyword '%s', forced its value to 'unknown'." % k)
                                header[k] = 'unknown'
                except IOError:
                    logging.error(
                        "Fits file '%s' could not be opened !" % fullpath)
                    header = None

                values = (path, size, json.dumps(header))
                try:
                    c.execute(sql, values) 
                except Exception as e:
                    logging.info(str(e))

                logging.info(f"Processed {path}")
    
    conn.commit()
    logging.info(f'{conn.total_changes} rows inserted' )
    conn.close()
        
    logging.info("Finished to update the FITS headers sqlite table.")

if __name__ == '__main__':
    main()
