# yafitssel

A `yafits` FITS file selection tool based where search requests consider the filesystem path, the size and the FITS header content in a syntax _à la_ SQL.
 
 `yafitssel` is a network application and all the interaction takes place in a WEB browser.

Example :
```
SELECT fitsinfos.Path FROM fitsinfos WHERE fitsinfos.Header.NAXIS1 > 1000 AND fitsinfos.Header.NAXIS2 > 1000 AND fitsinfos.Header.OBSERVER like "%doe%"
```


## Starting the application on the server
```
cd <yafitssel-directory>
PORT=3023 nohup ./bin/www &
```

- The port number can be adapted at your convenience
- The server's activity is logged in `<yafitssel-directory>/nohup.out`

## Using the application
In a WEB browser open `<server>:<port-number>` where `<server>` is the dns name of the server and `<port-number>` the value used at application start.

## Technical details
* `yafitssel` is a NodeJS application

* The underlying SQL database system is SQLite and the database used by the application is stored in the file `<yafitssel-directory>/fitsinfos.db`

* The dependancy of the SQLite database from the FITS files collection can be depicted as follows :

```
┌────────────────┐                              ┌────────────────────┐
│  FITS files    │                              │ SQLite database    │
│  top level     │                              │ stores FITS files  │
│  directory     │                              │ * location         │
├────────────────┤                              │ * size             │
│                │                              │ * header fields    │
│                │                              │                    │
│                │                              ├────────────────────┤
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │    The SQLite database       │                    │
│                │    to be updated to reflect  │                    │
│                │    the changes in the        │                    │
│                │    collection of FITS files  │                    │
│                │  ──────────────────────────► │                    │
│                │                              │                    │
│                │    script : fs2sqlite.py     │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
│                │                              │                    │
└────────────────┘                              └────────────────────┘
```
